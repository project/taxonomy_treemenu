<?php

/**
 * @file taxonomy_treemenu_view_fields.tpl.php
 * Default template to display a list of fields.
 *
 * @ingroup taxonomy_treemenu_templates
 */
?>

  <div class="taxonomy-treemenu-row-node">
      <span class="field-title">
      <?php print l($title, $node_url) ?> 
      </span>
  </div>
